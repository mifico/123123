public with sharing class ScheduleTestBatch implements Schedulable{
   String addText;
    
    public void execute(SchedulableContext ctx) {
        //System.abortJob(ctx.getTriggerId());
        
       /* if (this.isActiveBatchApexJobs()) {
            Datetime sysTime = System.now().addSeconds(5);
            String chronExpression = sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            System.schedule('ScheduleTestBatch' + sysTime.millisecond(), chronExpression, new ScheduleTestBatch());
        } else {   */         
            Database.executeBatch(new BatchTestLimits());
        //} 
    }
    
    private Boolean isActiveBatchApexJobs() {
        Integer count = [
                SELECT COUNT()
                FROM AsyncApexJob
                WHERE JobType = 'BatchApex' AND Status IN ('Processing', 'Queued', 'Preparing')
        ];
        
        return count > 5;
    }
}