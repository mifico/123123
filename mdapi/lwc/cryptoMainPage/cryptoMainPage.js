import { LightningElement, api, track } from 'lwc';
import encrypt from '@salesforce/apex/CryptoMainPage.encrypt';
import decrypt from '@salesforce/apex/CryptoMainPage.decrypt';
import getContacts from '@salesforce/apex/TestClass.getContacts';    


export default class CryptoMainPage extends LightningElement {
    inputValue;
    blobObj;    
    @track tableData;
    page = 1;
    numberOfRecords = 15;

    columns = [
        { label: 'Name', fieldName: 'Name' },
        { label: 'Title', fieldName: 'Title' },
        { label: 'Phone', fieldName: 'Phone' },        
        { label: 'Email', fieldName: 'Email', type: 'email' }
    ];

    btnClick(event){
        console.log("btn value: " + this.inputValue);  
        encrypt({inputText : this.inputValue})
        .then(result => {
            console.log("decr result: " + JSON.stringify(result));

            //let resMap = JSON.parse(result);
            //return decrypt({blobObj: result.encrypted, key: result.key});
        })
        // .then(result => {   
        //     console.log("decrypted res: " + result);
        // })
        .catch(error => {
            console.log("error" + JSON.stringify(error));
        })
    }

    handleInput(event){
        this.inputValue = event.target.value;
    }

    prekolHandler(){
        console.log("zlp");
    }

    getContactsPagination(){
        getContacts({startFrom: this.page, recordsDisplay: this.numberOfRecords})
        .then(result => {
            console.log("contacts: " + result);
            this.tableData = JSON.parse(result);
        })
        .catch(error => {
            console.log(JSON.stringify(error));
        })
    }

    nextPage(){
        this.page = this.page + 1;
        this.getContactsPagination();

    }

    previousPage(){
        this.page = this.page - 1;
        this.getContactsPagination();
    }

    connectedCallback(){
        console.log("connected callback");
        this.getContactsPagination();
    }
}