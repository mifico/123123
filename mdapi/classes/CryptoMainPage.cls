public with sharing class CryptoMainPage {
    
    @AuraEnabled
    public static String encrypt(String inputText){
        Blob key = Crypto.generateAesKey(128);
        Blob data = Blob.valueOf(inputText);
        Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, data); 
        
        String res = decrypt(encrypted, key);
        return 'encrypted res: ' + res;
        // return new Map<String, Blob>{
        //     'key' => key,
        //     'encrypted' => encrypted
        // }; 
    }

    @AuraEnabled
    public static String decrypt(Blob blobObj, Blob key){
        Blob decrypted = Crypto.decryptWithManagedIV('AES128', key, blobObj);
        String decryptedString = decrypted.toString();
        return decryptedString;
    }
}